# Be sure to restart your server when you modify this file.

# Define an application-wide content security policy
# For further information see the following documentation
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy

 Rails.application.config.content_security_policy do |policy|
   policy.default_src :self, :https
   policy.font_src    :self, :https, :data
   policy.img_src     :self, :https, :data
   policy.object_src  :none
   policy.script_src  :self, :https
   policy.style_src   :self, :https

#   # Specify URI for violation reports
#   # policy.report_uri "/csp-violation-report-endpoint"
 end

# If you are using UJS then enable automatic nonce generation
# Rails.application.config.content_security_policy_nonce_generator = -> request { SecureRandom.base64(16) }

# Report CSP violations to a specified URI
# For further information see the following documentation:
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only
# Rails.application.config.content_security_policy_report_only = true
#SecureHeaders::Configuration.default do |config|
#    config.csp = {
#      default_src: %w('none'), # nothing allowed by default
#      script_src: %w(cdn.example.com),
#      connect_src: %w('self'),
#      img_src: %w(cdn.example.com data:),
#      font_src: %w(cdn.example.com data:),
#      base_uri: %w('self'),
#      style_src: %w('unsafe-inline' cdn.example.com),
#      form_action: %w('self'),
#      report_uri: %w(/mgmt/csp_reports)
#    }
#  end