class User < ApplicationRecord

    validates :name, :email, :age, presence: true
    validates :name, length: { minimum: 2, maximum: 50 }, format: { with: /\A[^0-9`!@#\$%\^&*+_=]+\z/, message: "Only allows letters" }
    validates :age, numericality: { only_integer: true }, length: { maximum: 3 }
    validates :email, uniqueness: true, on: :create, format: { with: URI::MailTo::EMAIL_REGEXP }, length: { maximum: 50 }

end
