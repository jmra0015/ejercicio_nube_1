# README
1-Autores:  Sudheer Kumar Bethapudi y Juan Manuel Rey Alvarez

2-Detalles de la pila:  En esta primera versión se usa Ruby on Rails y Sqlite3

3-Ejemplos:  En las próximas versiones se actualizará este punto a medida que se incorporen

4-Pasos para lanzar el contenedor:

- En la carpeta donde se pongan todos los archivos se usa para desplegar la aplicación:
    docker-compose up

- Puede ser necesario si realizas algún cambio en los archivo Gemfile o de Docker hacerlo de la siguiente forma:
    docker-compose build
    docker-compose up

5-Dirección de GC
    http://35.185.21.39:3000/

6-Se realizó una aplicación, con un poco más de complejidad para que se pudiera ver desplegada en otra nube, en este caso una especializada en aplicaciones desarrolladas en rails.

https://jm-sample-app.herokuapp.com/

En la misma se pusieron en práctica todas las medidas de protección que se prácticaron durante el desarrollo de las prácticas con la aplicación sencilla como se recomendó.

Nota: Se deja la instancia activa, cualquier problema por favor notificar. Muchas gracias